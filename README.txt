Digital Fauvel Representation
Rebecca Fiebrink
Alan Thorne
4/22/2013

-----------------------------------------------------------------------

Contents:
 - Introduction
 - Layout File
	> Introduction
	> Description of tags
		~ facsimile
		~ surface
		~ graphic
		~ zone
	> ID assignment conventions
 - Content File
	> Introduction
	> Description of tags
		~ pb
		~ notatedMusic
		~ p (specific usage)
		~ figure
		~ lg & l
	> ID assignment conventions
	
-----------------------------------------------------------------------

INTRODUCTION:
	These files (Fauvel_Content.xml & Fauvel_Layout.xml) were created
	as part of the DigitalFauvel project at Princeton University. They
	are intended to be examples of the representation scheme used in
	the project. Some tag naming conventions come from the work done by
	The Text Encoding Initiative (www.tei-c.org). IDs are made to
	overlap between files so that they remain parallel and easy to
	maintain.

-----------------------------------------------------------------------
	
LAYOUT FILE: Fauvel_Layout.xml
	Introduction:
	The Layout file contains a physical description of the manuscript.
	What is found on each folio? Where is it located? Of which type is
	the content? This file contains no specific information about any
	of the content beyond its identification as "text", "image" or
	"music".
	
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	Description of tags:
		facsimile: The facsimile tag is a relic of this project's more
		direct association with TEI in the past. It serves no specific
		program purpose. It does signify, however, that this document
		is intended to act as a scan of the document, concerned with
		the documents visual apperance and not the specific nature of
		the content.
		
		surface: Each surface represents one page (recto or verso) of
		the Fauvel.
		
		graphic: A graphic is an image (in general). They are used here
		to locate the high resolution scans of the page in question.
		
		zone: Each zone is a bounding box indicating the on-screen
		location of content from the content file. Content type can be
		determined by the xml:id field.
	
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	ID assignment conventions: IDs are assigned in the following way.
	The page is	identified by number and side. For example, "10r" means
	that the particular tag is on page 10 recto. For each surface, this
	is the extent of the ID. For zones representing the whole page, a 
	"_p" is	concatenated to the page ID. This zone on page 10 recto
	would be "10r_p". Each motet and image on a page are indexed from
	1. The content type and index are concatenated with the page ID.
	The first image on page 10 recto would be IDed as "10rIm1". The 
	third motet	would be IDed as "10rMo3". Text is given its ID only
	from the line numbers found in a particular zone. A zone with lines
	196 to 254 would receive the ID "Te_0196-0254". 4 digits are always
	used.
	
	Recap:
		Content type	Example ID
		-----------------------------------------------
		surface:		10r
		zone (page):	10r_p
		zone (music):	10rMo2 (where "2" is the index)
		zone (image):	10rIm5 (where "5" is the index)
		text:			Te_0001-0010 (where "0001" is the first line
									  number and "0010" is the last
									  line number)

-----------------------------------------------------------------------
	
CONTENT FILE: Fauvel_Content.xml
	Introduction: The Content file contains the specific content
	of the Fauvel. This content file is based on the original document.
	A translation would be in a similar file.
	
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	Description of tags:
		pb: The pb (pagebreak) tag is placed at the beginning of each
		page of content. The facs pointer points to the same page in
		the Layout file.
		
		notatedMusic: This tag indicates music. The ptr tag points to
		files and file readers which could be used for the music at
		this location. See the tei P5 guidelines (www.tei-c.org) for
		more information about pointing.

		p: Paragraph tags are used to indicate lyrics to previously
		specified motets.
		
		figure & figDesc: The figure tag indicates the presence of an
		image. Since we do not expect any useful overlays for images,
		the only property is figDesc, a description of the figure.
		
		lg & l: The lg tag indicates a line group for poetry. Each line
		is placed in l tags. The n field of each line indicates the
		line number (within the Fauvel, not locally).
		
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		
	ID assignment conventions: IDs are assigned in similar manner to
	analagous tags in the Layout file (see "Layout file>ID assigment 
	conventions" above for more information). pb:facs tags are
	assigned by	the number and position of the page they are
	representing. A pb which points to page 10 recto would be given a
	facs value of "#10r". notatedMusic tags derive their IDs from the
	page number and side, their status as music, and the index of this
	particular movement. The first motet on page 10 recto would receive
	the ID "10rMo1". The p which holds the lyrics uses the same ID with
	the addition of "_t" (for "text") at the end. The lyrics to the
	aforementioned notatedMusic tag would be "10rMo1_t". Images are
	IDed by the page, side, and image index. The fourth image on page
	10 recto would be "10rIm1". lg tags are IDed by the start and end
	line numbers. A block of text containing lines 4 through 119 would
	be given the ID "Te_0004-0119". 4 digits are always used.
	
	Recap:
		Content type	Example ID
		-----------------------------------------------
		pb:facs:		#10r
		notatedMusic:	10rMo2 (where "2" is the index)
		p (music):		10rMo2_t (where "2" is the index)
		figure:			10rIm5 (where "5" is the index)
		lg:				Te_0001-0010 (where "0001" is the first line
									  number and "0010" is the last
									  line number)